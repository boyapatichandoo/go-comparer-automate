package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/tcnksm/go-httpstat"
)

//ResponseData ...
type ResponseData struct {
	requestOneData        interface{}
	responseOneStatusCode string
	requestTwoData        interface{}
	responseTwoStatusCode string
	requestOneLatency     string
	requestTwoLatency     string
}

type responseStatus struct {
	response       ResponseData
	isSameResponse bool
	errOne         error
	errTwo         error
}

//RequestDetail holds the properties requires to send request
type RequestDetail struct {
	URL            string   `json:"url"`
	Method         string   `json:"method"`
	Headers        []Header `json:"headers"`
	Body           string   `json:"body"`
	RequiredFields []string
}

// ExcelRequestDetail ...
type ExcelRequestDetail struct {
	Request            RequestDetail
	ResponseTimeCell   string
	IssuesCell         string
	IsResponseSameCell string
	ResponseCell       string
	StatusCell         string
	CanMakeRequest     bool
}

// Header ...
type Header struct {
	Key   string
	Value string
}

var wg sync.WaitGroup

func makeRequestsandCompareData(requestOne, requestTwo RequestDetail) responseStatus {
	wg.Add(2)
	var res1 []byte
	var res2 []byte
	var responseOneLatency, responseTwoLatency string
	var responseOneStatusCode, responseTwoStatusCode string
	var err1, err2, err error
	response := ResponseData{
		requestOneData:    "",
		requestTwoData:    "",
		requestOneLatency: "0",
		requestTwoLatency: "0",
	}
	go func() {
		res1, responseOneLatency, responseOneStatusCode, err1 = getResponse(requestOne)
	}()

	go func() {
		res2, responseTwoLatency, responseTwoStatusCode, err2 = getResponse(requestTwo)
	}()
	wg.Wait()
	response.requestOneLatency = responseOneLatency
	response.responseOneStatusCode = responseOneStatusCode
	response.requestTwoLatency = responseTwoLatency
	response.responseTwoStatusCode = responseTwoStatusCode
	// fmt.Println(string(res1), string(res2))
	// fmt.Println(err1, err2)

	var resOne = make(map[string]interface{})
	var resTwo = make(map[string]interface{})
	var resOneBytes = make([]byte, 0)
	var resTwoBytes = make([]byte, 0)

	if err1 != nil {
		// fmt.Println("Errors:", err1)
		response.requestOneData = err1.Error()
		// return responseStatus{response, false, err1}
	} else {
		if err = json.Unmarshal(res1, &response.requestOneData); err != nil {
			// fmt.Println("Errors1:", err)
			response.requestOneData = string(res1)
			// return responseStatus{response, false, err}
		} else {
			if err = json.Unmarshal(res1, &resOne); err != nil {
				fmt.Println("map unmarshal 1 err", err)
			} else {
				correlationIdKey := "correlationId"
				correlationIdValue := ""
				if cId, exists := resOne[correlationIdKey]; exists {
					if value, ok := cId.(string); ok {
						correlationIdValue = value
					}
				} else if cId, exists := resOne["correlationid"]; exists {
					correlationIdKey = "correlationid"
					if value, ok := cId.(string); ok {
						correlationIdValue = value
					}
				}
				if requestOne.RequiredFields != nil && len(requestOne.RequiredFields) > 0 {
					// fmt.Println("alter", resOne)
					removeOtherFields(resOne, requestOne.RequiredFields)
					// fmt.Println("alter",  string(res1))
					// setFloatPrecision(resOne)

				}
				setFloatPrecision(resOne, string(res1))

				delete(resOne, correlationIdKey)
				delete(resOne, "messageId")
				delete(resOne, "messageId_exception")
				resOneBytes, err = json.Marshal(resOne)
				if err != nil {
					fmt.Println(err)
				}
				resOne[correlationIdKey] = correlationIdValue
				contents, err := json.MarshalIndent(resOne, "", "\t")
				file, err := os.Create("response/" + correlationIdValue + ".json")
				if err != nil {
					fmt.Println("File Write Error:", err)
				}
				_, err = file.WriteString(string(contents))
				if err != nil {
					fmt.Println("File Write Error:", err)
				} else {
					fmt.Println("Response added to file")
				}
				response.requestOneData = resOne
				b, err := json.Marshal(resOne)
				if err != nil {
					fmt.Println("res1 marhsal error:", err)
				} else {
					response.requestOneData = string(b)
				}
				// response.requestOneData = resOne
				// if sm, ok :=  response.requestOneData.(map[string]interface{}); ok {
				//  sm["data"] = string(res1)
				//  fmt.Printf("%T\n",sm["data"])
				// }
			}
		}
	}
	// setFloatPrecision(resOne)

	// if sm, ok :=  resOne["data"].([]interface{}); ok {
	//  if m, ok :=  sm[0].(map[string]interface{}); ok {
	//      fmt.Printf("%T\n",m["allowedAmount"])
	//  }
	//  fmt.Printf("%T\n",sm[0])
	// }

	if err2 != nil {
		// fmt.Println("Errors:", err2)
		response.requestTwoData = err2.Error()
		// return responseStatus{response, false, err2}
	} else {
		if err = json.Unmarshal(res2, &response.requestTwoData); err != nil {
			// fmt.Println("Errors2:", err)
			response.requestTwoData = string(res2)
			// return responseStatus{response, false, err}
		} else {
			if err = json.Unmarshal(res2, &resTwo); err != nil {
				fmt.Println("map unmarshal 2 err", err)
			} else {
				correlationIdKey := "correlationId"
				correlationIdValue := ""
				if cId, exists := resTwo[correlationIdKey]; exists {
					// fmt.Println("coerrid", cId, resTwo)
					if value, ok := cId.(string); ok {
						correlationIdValue = value
					}
				} else if cId, exists := resTwo["correlationid"]; exists {
					correlationIdKey = "correlationid"
					if value, ok := cId.(string); ok {
						correlationIdValue = value
					}
				}
				if requestTwo.RequiredFields != nil && len(requestTwo.RequiredFields) > 0 {
					// fmt.Println("alter", resTwo)
					removeOtherFields(resTwo, requestTwo.RequiredFields)
					// fmt.Println("alter", resTwo)
					response.requestTwoData = resTwo
				}
				setFloatPrecision(resTwo, string(res2))

				delete(resTwo, correlationIdKey)
				delete(resTwo, "messageId")
				delete(resTwo, "messageId_exception")
				resTwoBytes, err = json.Marshal(resTwo)
				if err != nil {
					fmt.Println(err)
				}
				resTwo[correlationIdKey] = correlationIdValue
				contents, err := json.MarshalIndent(resTwo, "", "\t")
				file, err := os.Create("response/" + correlationIdValue + ".json")
				if err != nil {
					fmt.Println("File Write Error:", err)
				}
				_, err = file.WriteString(string(contents))
				if err != nil {
					fmt.Println("File Write Error:", err)
				} else {
					fmt.Println("Response added to file")
				}
				response.requestTwoData = resTwo
				b, err := json.Marshal(resTwo)
				if err != nil {
					fmt.Println("res2 marhsal error:", err)
				} else {
					response.requestTwoData = string(b)
				}
				// response.requestOneData = resOne
				// if sm, ok :=  response.requestOneData.(map[string]interface{}); ok {
				//  sm["data"] = string(res1)
				//  fmt.Printf("%T\n",sm["data"])
				// }
			}
		}
	}

	// fmt.Println("Same Respone:", response)

	// fmt.Println(string(contents))

	// diffCheck(struct{ requestOneData, MuleData interface{} }{requestOneData: response.requestOneData, MuleData: response.MuleData})
	return responseStatus{response, bytes.Equal(resOneBytes, resTwoBytes), err1, err2}
}

func getResponse(request RequestDetail) ([]byte, string, string, error) {
	defer wg.Done()
	client := &http.Client{}
	// fmt.Println(request.Body)
	headerMap := make(http.Header)
	for _, obj := range request.Headers {
		// fmt.Println(obj.Key, ":", obj.Value)
		headerMap.Set(strings.TrimSpace(obj.Key), strings.TrimSpace(obj.Value))
	}
	var requestBody io.Reader = nil
	if strings.ToUpper(request.Method) == "POST" || strings.ToLower(request.Method) == "PUT" {
		bodyMode := headerMap.Get("Content-Type")
		if bodyMode == "application/x-www-form-urlencoded" {
			body := strings.Split(request.Body, "\n")
			var bodyData = url.Values{}
			for _, value := range body {
				item := strings.Split(value, ":")
				if len(item) == 2 {
					bodyData.Set(item[0], item[1])
				}
			}
			requestBody = bytes.NewBuffer([]byte(bodyData.Encode()))
		} else {
			// jsonBody, err := json.Marshal(request.Body)
			// if err != nil {
			//  jsonBody = nil
			// }
			requestBody = bytes.NewBuffer([]byte(request.Body))
		}
	}
	requestURL := strings.TrimSuffix(strings.Trim(request.URL, " "), "\n")
	requestURL = strings.ReplaceAll(requestURL, " ", "%20")
	req, err := http.NewRequest(request.Method, requestURL, requestBody)
	if err != nil {
		fmt.Println("Request", request.URL, err)
	}
	req.Header = headerMap

	response := make([]byte, 0)
	// fmt.Println("request", req)
	var result httpstat.Result
	ctx := httpstat.WithHTTPStat(req.Context(), &result)
	req = req.WithContext(ctx)
	requestedAt := time.Now()
	res, err := client.Do(req)
	// fmt.Println("response", res, err)
	latency := time.Since(requestedAt) //.String()
	// fmt.Println(result)
	// fmt.Println("result", result.DNSLookup.Milliseconds(),result.TCPConnection.Milliseconds(),result.TLSHandshake.Milliseconds(),result.ServerProcessing.Milliseconds(),int64(result.ContentTransfer(time.Now())/time.Millisecond))
	var contentTransfer = int64(result.ContentTransfer(time.Now()) / time.Millisecond)
	if contentTransfer > 60000 {
		contentTransfer = 0
	}
	// latency := result.DNSLookup.Milliseconds()+result.TCPConnection.Milliseconds()+result.TLSHandshake.Milliseconds()+result.ServerProcessing.Milliseconds()+contentTransfer
	var responseLatency string
	latencyString := strconv.FormatFloat(float64(latency), 'E', 2, 64)
	roundedUpLatency, parseErr := strconv.ParseFloat(latencyString, 64)
	if parseErr != nil {
		responseLatency = latency.String()
		// responseLatency = fmt.Sprintf("%dms", latency)
	}
	// responseLatency = fmt.Sprintf("%dms", latency)
	// responseLatency = fmt.Sprintf("%s", result.Total(time.Now()))
	// fmt.Println(responseLatency, "roundedUpLatencyr",roundedUpLatency)
	responseLatency = time.Duration(roundedUpLatency).String()
	if err != nil {
		response = []byte(err.Error())
		return response, responseLatency, fmt.Sprintf("%v", http.StatusNotFound) + " NotFound", err
	}
	defer res.Body.Close()
	contents, err := ioutil.ReadAll(res.Body)
	if err != nil {
		response = []byte(err.Error())
		return response, responseLatency, res.Status, err
	}
	// file, err := os.Create(req.RequestURI + ".json")
	// if err != nil {
	//  fmt.Println("File Write Error:", err)
	// }
	// _, err = file.WriteString(string(contents))
	// if err != nil {
	//  fmt.Println("File Write Error:", err)
	// } else {
	//  fmt.Println("Response added to file")
	// }
	// response = contents
	return contents, responseLatency, res.Status, nil
}

func removeOtherFields(data interface{}, fields []string) {
	if data != nil {
		if mapData, ok := data.(map[string]interface{}); ok {
			for key, value := range mapData {
				nestedFields := make([]string, 0)
				removeFlag := true
				for _, field := range fields {
					props := strings.Split(field, ".")
					// fmt.Println(key, "props######################", props)
					if key == strings.TrimSpace(props[0]) {
						if len(props) == 1 {
							removeFlag = false
							break
						}
						nestedFields = append(nestedFields, strings.Join(props[1:], "."))
						// fmt.Println("nestedFields@@@@@@@@@@@@@@@@@@@@@", nestedFields)
					}
				}
				if !removeFlag {
					continue
				}
				if len(nestedFields) > 0 {
					removeOtherFields(value, nestedFields)
				} else {
					delete(mapData, key)
				}
			}
		} else if sliceData, ok := data.([]interface{}); ok {
			for _, element := range sliceData {
				removeOtherFields(element, fields)
			}
		}
	}
}

func setFloatPrecision(data interface{}, resString string) {
	// fmt.Println("data", reflect.TypeOf(data).Name())
	if data != nil {
		if mapData, ok := data.(map[string]interface{}); ok {
			for key, value := range mapData {
				// fmt.Println(reflect.TypeOf(mapData[key]).Name())
				// if reflect.TypeOf(value).Name() == "float64" {
				//  floatValue := strconv.FormatFloat(value.(float64), 'f', 2, 64)
				//  fmt.Println("floatValue", key, floatValue)
				//  if floatValue[len(floatValue)-1:] == "0" {
				//      mathcedRegex := `"` + key + `":` + strconv.FormatFloat(value.(float64), 'f', 2, 64)
				//      keyValue := regexp.MustCompile(mathcedRegex).FindString(resString)
				//      fmt.Println(mathcedRegex, keyValue)
				//      if keyValue != "" {
				//          // mathcedRegex := `"b:"` + strconv.FormatFloat(value.(float64))
				//          // fileDataString = strings.Replace(fileDataString, `"a"`+":"+strconv.FormatFloat(value.(float64), 'f', -1, 64)+",", "", 1)
				//          // fmt.Println(`"a":`+strconv.FormatFloat(value.(float64), 'f', -1, 64), fileDataString)
				//          mapData[key] = Currency(value.(float64))
				//          fmt.Println(reflect.TypeOf(mapData[key]).Name())
				//      }
				//  }

				// }
				// setFloatPrecision(value)
				// fmt.Println(reflect.TypeOf(value), value)
				if value != nil && reflect.TypeOf(value).Name() == "float64" {
					// fmt.Println(key, value, reflect.TypeOf(value).Name())
					// // fmt.Printf("%T\n", mapData[key])
					// mapData[key] = Currency(value.(float64))
					// // fmt.Printf("%T\n", mapData[key])
					floatValue := strconv.FormatFloat(value.(float64), 'f', 2, 64)
					// fmt.Println("floatValue", key, floatValue)
					if floatValue[len(floatValue)-1:] == "0" {
						mathcedRegex := `"` + key + `": ` + strconv.FormatFloat(value.(float64), 'f', 2, 64)
						keyValue := regexp.MustCompile(mathcedRegex).FindString(resString)
						if keyValue == "" {
							mathcedRegex := `"` + key + `":` + strconv.FormatFloat(value.(float64), 'f', 2, 64)
							keyValue = regexp.MustCompile(mathcedRegex).FindString(resString)
						}
						// fmt.Println(mathcedRegex, keyValue)
						if keyValue != "" {
							// mathcedRegex := `"b:"` + strconv.FormatFloat(value.(float64))
							// fileDataString = strings.Replace(fileDataString, `"a"`+":"+strconv.FormatFloat(value.(float64), 'f', -1, 64)+",", "", 1)
							// fmt.Println(`"a":`+strconv.FormatFloat(value.(float64), 'f', -1, 64), fileDataString)
							mapData[key] = Currency(value.(float64))
							// fmt.Println(reflect.TypeOf(mapData[key]).Name())
						}
					}
				} else if value != nil && reflect.TypeOf(value).Name() == "float32" {
					fmt.Println(key, value, reflect.TypeOf(value).Name())
					// fmt.Printf("%T\n", mapData[key])
					mapData[key] = Currency(value.(float32))
					// fmt.Printf("%T\n", mapData[key])
				} else {
					setFloatPrecision(value, resString)
				}
			}
		} else if sliceData, ok := data.([]interface{}); ok {
			for _, element := range sliceData {
				setFloatPrecision(element, resString)
			}
		}
	}
}

type Currency float64

//MarshalJSON Marshals the Currency type
func (c Currency) MarshalJSON() ([]byte, error) {
	// fmt.Println("float marshal", string([]byte(fmt.Sprintf("%.2f", c))))
	return []byte(fmt.Sprintf("%.2f", c)), nil
}
