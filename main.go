package main

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/boyapatichandoo/service-compare/models"

	"github.com/gorilla/mux"
)

var serviceListDetail models.ServiceListDetail

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", cmpPage)
	router.HandleFunc("/getSeviceData", getServiceData).Methods(http.MethodGet)
	router.HandleFunc("/cmp", selfRequestCompare).Methods(http.MethodPost)

	http.ListenAndServe(":8000", router)
}

func cmpPage(w http.ResponseWriter, r *http.Request) {
	template, err := template.ParseFiles("compare.html")
	if err != nil {
		log.Panicln("template parse", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error occured while parsing UI"))
		return
	}
	template.Execute(w, nil)
}

func getServiceData(w http.ResponseWriter, r *http.Request) {
	fileDataBytes, err := ioutil.ReadFile("service-list.json")
	if err != nil {
		log.Println("file data read", err)
		w.Write([]byte("error occured while parsing service list  data"))
		return
	}
	err = json.Unmarshal(fileDataBytes, &serviceListDetail)
	if err != nil {
		log.Println("file data unmarshal", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error while parsing service list  data"))
		return
	}
	resBytes, err := json.Marshal(serviceListDetail)
	if err != nil {
		log.Println("service list data marshal", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error while parsing service list  data"))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resBytes)
}

func selfRequestCompare(w http.ResponseWriter, r *http.Request) {
	// fmt.Println("SERVICE", serviceListDetail)
	comparisonRequestData := []byte(r.FormValue("data"))
	// fmt.Println(string(comparisonRequestData))
	comaparisonRequestList := make([]models.ComparisonRequest, 0)
	err := json.Unmarshal(comparisonRequestData, &comaparisonRequestList)
	if err != nil {
		log.Println("request data parse", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error occured while parsing request data"))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	// fmt.Println(comaparisonRequestList)
	createRequests(comaparisonRequestList, w)

	// w.Write(comparisonRequestData)
}
