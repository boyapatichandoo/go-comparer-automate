package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/godror/godror"
)

func ExecuteQuery(queryString string) ([]map[string]interface{}, error) {
	db := OpenConnection()
	resRows, err := db.Query(queryString)
	if err != nil{
		log.Println("DB Connection Open", err)
		return nil, err
	}
	columns, err := resRows.Columns()
	if err != nil{
		log.Println("Query Columns read", err)
		return nil, err
	}
	return scanRecords(columns, resRows)
}
func OpenConnection() *sql.DB {
	connectionString := getConnectionString()
	db, err := sql.Open("godror", connectionString)
	if err != nil{
		log.Fatal("DB Connection Open", err)
	}
	return db
}

func getConnectionString() string{
	// Connection String Format - username/password@host:port/serviceName
	connectionString := fmt.Sprintf("%[1]s/%[2]s@%[3]s:%[4]d/%[5]s", "username", "password", "host", "port", "servicename")
	return connectionString
}
func scanRecords(columns []string, rows *sql.Rows) ([]map[string]interface{}, error) {

	rowValues := make([]interface{}, len(columns))   // a slice of interface{}'s to represent values of each row.
	valPointers := make([]interface{}, len(columns)) // a slice of interface{}'s to contain pointers to each item in the rowValues slice.
	for i := 0; i < len(columns); i++ {
		valPointers[i] = &rowValues[i]
	}

	records := make([]map[string]interface{}, 0)

	// loop rows
	for rows.Next() {

		// scan row to valPointers
		if err := rows.Scan(valPointers...); err != nil {
			return nil, err
		}

		// row vals
		row := make(map[string]interface{})
		for i, v := range rowValues {
			row[columns[i]] = v
		}
		records = append(records, row)
	}

	return records, nil
}