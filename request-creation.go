package main

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/boyapatichandoo/service-compare/models"
)

func createRequests(requestData []models.ComparisonRequest, w http.ResponseWriter) {
	var cmpReqs = 0
	for _, req := range requestData {
		for _, uriParam := range req.UriParams {
			cmpReqs += uriParam.Count
		}
		if len(req.UriParams) == 0 {
			cmpReqs++
		}
	}
	// fmt.Println(cmpReqs)
	comparisonList := make([]models.ComparisonEntity, 0, cmpReqs)
	var m = &sync.Mutex{}
	var wg = &sync.WaitGroup{}
	// fmt.Println("requestData", requestData)
	for _, request := range requestData {
		wg.Add(1)
		func(sID, rId int, uriParams []models.URIParam, comparisonList *[]models.ComparisonEntity) {
			defer wg.Done()
			if service, route, exists := getServiceRequestAndRoute(sID, rId); exists {
				createRequestWithParams(service, route, uriParams, comparisonList, m)
			}
			// fmt.Println("Comparison", comparisonList)
		}(request.ServiceId, request.RouteId, request.UriParams, &comparisonList)

	}
	wg.Wait()
	fmt.Println("Comparison", len(comparisonList))
	if len(comparisonList) > 0 {
		writeRequestComparisonData(comparisonList, requestData[0].AuthOne, requestData[0].AuthOne, serviceListDetail.AuthOneURL, serviceListDetail.AuthOneHeaderKey, serviceListDetail.AuthOneHeaderValue, serviceListDetail.AuthTwoURL, serviceListDetail.AuthTwoHeaderKey, serviceListDetail.AuthTwoHeaderValue)
		compareExcelRequests("test.xlsx", w)
	}

}

func getServiceRequestAndRoute(serviceID, routeID int) (models.Services, models.Routes, bool) {
	// fmt.Println("sId, rID", serviceID, " ", routeID, serviceListDetail)
	for _, service := range serviceListDetail.Services {
		if service.ID == serviceID {
			for _, route := range service.Routes {
				if route.ID == routeID {
					return service, route, true
				}
			}
		}
	}
	return models.Services{}, models.Routes{}, false
}

func createRequestWithParams(service models.Services, route models.Routes, uriparams []models.URIParam, comparisonList *[]models.ComparisonEntity, m *sync.Mutex) {
	// fmt.Println("uri params", uriparams)
	comparisionEntity := models.ComparisonEntity{
		RequestOne: models.ServiceRequest{
			Method: route.Method,
			URL:    strings.Replace(serviceListDetail.HostOne, "{{port}}", service.Port, 1) + route.RouteOne,
			// Headers: map[string]string{
			// 	serviceListDetail.AuthOneHeaderKey: serviceListDetail.AuthOneHeaderValue,
			// },
		},
		RequestTwo: models.ServiceRequest{
			Method: route.Method,
			URL:    strings.Replace(serviceListDetail.HostTwo, "{{port}}", service.Port, 1) + route.RouteTwo,
			// Headers: map[string]string{
			// 	serviceListDetail.AuthTwoHeaderKey: serviceListDetail.AuthTwoHeaderValue,
			// },
		},
	}
	if len(uriparams) == 0 {
		comparisionEntity.CanCompare = true
		m.Lock()
		defer m.Unlock()
		*comparisonList = append(*comparisonList, comparisionEntity)
		return
	}
	var uriParamValues = make([]models.URIParams, 0)
	for _, param := range uriparams {
		uriParam := models.URIParams{}
		uriParam.ID = param.UriParamID
		uriParam.Value = getURIParamReplacerString(route.URIParams, param.UriParamID)
		// fmt.Println(param.Count, param.CountNumber, "param.Count, param.CountNumber")
		uriParam.Values = getParamValueFromDB(route.Query, param.Count, param.CountNumber)
		// var params = getParamValueFromDB(route.Query, route.Name, strconv.Itoa(param.Count))

		// fmt.Println(&comparisonList, len(*comparisonList), cap(comparisonList))
		// fmt.Println("comp", comparisonList)
		// fmt.Println(&comparisonList, len(comparisonList), cap(comparisonList))
		uriParamValues = append(uriParamValues, uriParam)
	}
	addURIParamValueToRoute(uriParamValues, comparisionEntity, comparisonList, m)
}

func addURIParamValueToRoute(uriParams []models.URIParams, comparisionEntity models.ComparisonEntity, comparisonList *[]models.ComparisonEntity, m *sync.Mutex) {
	// fmt.Println("param DB", len(uriParams), uriParams)
	for _, value := range uriParams[0].Values {
		cmpEntity := comparisionEntity
		cmpEntity.RequestOne.URL = strings.Replace(comparisionEntity.RequestOne.URL, uriParams[0].Value, value, 1)
		cmpEntity.RequestTwo.URL = strings.Replace(comparisionEntity.RequestTwo.URL, uriParams[0].Value, value, 1)
		cmpEntity.CanCompare = true
		if len(uriParams) > 1 {
			addURIParamValueToRoute(uriParams[1:], cmpEntity, comparisonList, m)
		} else {
			m.Lock()
			*comparisonList = append(*comparisonList, cmpEntity)
			m.Unlock()
		}
	}
	if len(uriParams[0].Values) == 0 {
		comparisionEntity.Issue = "No Value found for " + uriParams[0].Value
		comparisionEntity.CanCompare = false
		m.Lock()
		*comparisonList = append(*comparisonList, comparisionEntity)
		m.Unlock()
	}
}
func getURIParamReplacerString(uriParams []models.URIParams, uriparamID int) string {
	for _, param := range uriParams {
		if param.ID == uriparamID {
			return param.Value
		}
	}
	return ""
}

func getParamValueFromDB(query string, count int, countNumber int) []string {
	// fmt.Println("count", count, countNumber)
	var params = make([]string, 0, count)
	if 1 == countNumber {
		for i := 1; i <= count; i++ {
			params = append(params, strconv.Itoa(i))
		}
	} else {
		startNum := count * (countNumber - 1)
		for i, j := startNum, 1; i < count*countNumber; i++ {
			params = append(params, strconv.Itoa(startNum+j))
			j++
		}
	}
	ExecuteQuery(query)
	return params
}
