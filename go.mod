module gitlab.com/boyapatichandoo/service-compare

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.3.0
	github.com/godror/godror v0.20.0
	github.com/gorilla/mux v1.7.4
	github.com/tcnksm/go-httpstat v0.2.0
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
)
