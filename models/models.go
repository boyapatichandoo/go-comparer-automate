package models

// ServiceRequest ...
type ServiceRequest struct {
	Method  string
	URL     string
	Headers map[string]string
}

// ComparisonEntity stores the two requests to compare
type ComparisonEntity struct {
	RequestOne        ServiceRequest
	RequestTwo        ServiceRequest
	ResponeOne        interface{}
	ResponeTwo        interface{}
	RequestOneLatency string
	RequestTwoLatency string
	IsSameResponse    bool
	Issue             string
	CanCompare        bool
}

// ServiceListDetail ....
type ServiceListDetail struct {
	HostOne            string     `json:"hostOne"`
	HostTwo            string     `json:"hostTwo"`
	AuthOneURL         string     `json:"authOneUrl"`
	AuthOneHeaderKey   string     `json:"authOneHeaderKey"`
	AuthOneHeaderValue string     `json:"authOneHeaderValue"`
	AuthTwoURL         string     `json:"authTwoUrl"`
	AuthTwoHeaderKey   string     `json:"authTwoHeaderKey"`
	AuthTwoHeaderValue string     `json:"authTwoHeaderValue"`
	Services           []Services `json:"services"`
}

// URIParams ....
type URIParams struct {
	ID           int      `json:"id"`
	Name         string   `json:"name"`
	Value        string   `json:"value"`
	Values       []string `json:"values"`
	DefaultValue int      `json:"defaultValue"`
	CountNumber  int      `json:"countNumber"`
}

// Routes ....
type Routes struct {
	ID        int         `json:"id"`
	Name      string      `json:"name"`
	RouteOne  string      `json:"routeOne"`
	URIParams []URIParams `json:"uriParams"`
	RouteTwo  string      `json:"routeTwo"`
	Method    string      `json:"method"`
	Query     string      `json:"query"`
}

// Services ....
type Services struct {
	ID     int      `json:"id"`
	Name   string   `json:"name"`
	Port   string   `json:"port"`
	Routes []Routes `json:"routes"`
}

// ComparisonRequest ...
type ComparisonRequest struct {
	ServiceId int
	RouteId   int
	AuthOne   bool
	AuthTwo   bool
	UriParams []URIParam
}

type URIParam struct {
	UriParamID  int
	Count       int
	CountNumber int
	value       []string
}
