package main

import (
	// "fmt"
	"log"
	"strconv"

	"gitlab.com/boyapatichandoo/service-compare/models"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
)

func writeRequestComparisonData(comparisonList []models.ComparisonEntity, authOne, authTwo bool, authOneUrl, authOneHeaderKey, authOneHeaderValue, authTwoUrl, authTwoHeaderKey, authTwoHeaderValue string) {
	var err error
	authOneNeeded, authTwoNeeded := "No", "No"
	if authOne {
		authOneNeeded = "Yes"
	}
	if authTwo {
		authTwoNeeded = "Yes"
	}
	xlsxFile := excelize.NewFile()
	// xlsxFile.shee
	// if err != nil {
	// 	log.Println(err)
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	w.Write([]byte("An error occured while opening file. Please try again"))
	// 	return
	// }
	// rows, err := xlsxFile.Rows("Sheet1")
	// if err != nil {
	// 	log.Println("rows read", err)
	// 	return
	// }

	borderFields := []excelize.Border{
		excelize.Border{Type: "left", Color: "000000", Style: 1},
		excelize.Border{Type: "bottom", Color: "000000", Style: 1},
		excelize.Border{Type: "top", Color: "000000", Style: 1},
		excelize.Border{Type: "right", Color: "000000", Style: 1},
	}
	colorStyle, err := xlsxFile.NewStyle(&excelize.Style{
		Fill:      excelize.Fill{Color: []string{"#5987d8"}, Type: "pattern", Pattern: 1},
		Border:    borderFields,
		Alignment: &excelize.Alignment{Horizontal: "center", Vertical: "center", WrapText: true},
	})
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet1", "A1", "M1", colorStyle)
	excelWriteErrorCheck(err)

	borderStyleVerticalAndHorizontalCenterStyle, err := xlsxFile.NewStyle(&excelize.Style{
		Border:    borderFields,
		Alignment: &excelize.Alignment{Horizontal: "center", Vertical: "center", WrapText: true},
	})
	excelWriteErrorCheck(err)
	borderStyleVerticalCenterStyle, err := xlsxFile.NewStyle(&excelize.Style{
		Border:    borderFields,
		Alignment: &excelize.Alignment{Vertical: "center", WrapText: true},
	})
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet1", "A2", "C"+strconv.Itoa(2*len(comparisonList)+1), borderStyleVerticalAndHorizontalCenterStyle)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet1", "D2", "D"+strconv.Itoa(2*len(comparisonList)+1), borderStyleVerticalCenterStyle)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellStyle("Sheet1", "E2", "M"+strconv.Itoa(2*len(comparisonList)+1), borderStyleVerticalAndHorizontalCenterStyle)
	excelWriteErrorCheck(err)
	// err = xlsxFile.SetCellStyle("Sheet1", "B1", "B1", colorStyle)
	// excelWriteErrorCheck(err)
	verticalCenterStyle, err := xlsxFile.NewStyle(&excelize.Style{Alignment: &excelize.Alignment{Vertical: "top", Horizontal: "left"}})
	excelWriteErrorCheck(err)
	// horizontalCenterStyle, err := xlsxFile.NewStyle(&excelize.Style{Alignment:&excelize.Alignment{Horizontal: "center"}})
	// excelWriteErrorCheck(err)

	verticalAndHorizontalCenterStyle, err := xlsxFile.NewStyle(&excelize.Style{Alignment: &excelize.Alignment{Horizontal: "center", Vertical: "center"}})
	excelWriteErrorCheck(err)

	err = xlsxFile.SetRowHeight("Sheet1", 1, 50)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetColStyle("Sheet1", "A:C", verticalAndHorizontalCenterStyle)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColStyle("Sheet1", "D", verticalCenterStyle)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColStyle("Sheet1", "E:I", verticalAndHorizontalCenterStyle)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColWidth("Sheet1", "A", "C", 10)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColWidth("Sheet1", "D", "D", 100)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColWidth("Sheet1", "E", "J", 20)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColWidth("Sheet1", "K", "K", 100)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColWidth("Sheet1", "L", "L", 20)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetColWidth("Sheet1", "M", "M", 50)
	excelWriteErrorCheck(err)
	// err = xlsxFile.SetColStyle("Sheet1", "F", verticalCenterStyle)
	// excelWriteErrorCheck(err)
	// err = xlsxFile.SetColStyle("Sheet1", "G:I", verticalAndHorizontalCenterStyle)
	// excelWriteErrorCheck(err)
	// // err = xlsxFile.SetColStyle("Sheet1", "H", verticalAndHorizontalCenterStyle)
	// // excelWriteErrorCheck(err)
	// // err = xlsxFile.SetColStyle("Sheet1", "I", verticalAndHorizontalCenterStyle)
	// // excelWriteErrorCheck(err)
	// err = xlsxFile.SetColStyle("Sheet1", "J:K", verticalCenterStyle)
	// excelWriteErrorCheck(err)
	// // err = xlsxFile.SetColStyle("Sheet1", "K", verticalCenterStyle)
	// // excelWriteErrorCheck(err)

	// err = xlsxFile.SetColStyle("Sheet1", "L", verticalAndHorizontalCenterStyle)
	// excelWriteErrorCheck(err)
	// err = xlsxFile.SetColStyle("Sheet1", "M", verticalCenterStyle)
	// excelWriteErrorCheck(err)

	// err = xlsxFile.SetColStyle("Sheet1", "A:M", colorStyle)
	// excelWriteErrorCheck(err)
	{
		err = xlsxFile.SetCellValue("Sheet1", "A1", "S.No")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "B1", "Endpoint")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "C1", "Method")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "D1", "URL")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "E1", "Headers")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "F1", "RequestBody")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "G1", "NeedAuthorization")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "H1", "IsResponeSame")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "I1", "ResponseTime")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "J1", "Issues")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "K1", "Response")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "L1", "Statux")
		excelWriteErrorCheck(err)

		err = xlsxFile.SetCellValue("Sheet1", "M1", "Required Response Fileds")
		excelWriteErrorCheck(err)
	}

	rowCount := 1
	for index, comparison := range comparisonList {
		// fmt.Println("one", rowCount, index + 1)
		err = xlsxFile.SetRowHeight("Sheet1", rowCount+1, 50)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "A"+strconv.Itoa(rowCount+1), rowCount)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "B"+strconv.Itoa(rowCount+1), index+1)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "C"+strconv.Itoa(rowCount+1), comparison.RequestOne.Method)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "D"+strconv.Itoa(rowCount+1), comparison.RequestOne.URL)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "G"+strconv.Itoa(rowCount+1), authOneNeeded)
		excelWriteErrorCheck(err)
		rowCount++

		// fmt.Println("two", rowCount, index + 1)
		err = xlsxFile.SetRowHeight("Sheet1", rowCount+1, 50)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "A"+strconv.Itoa(rowCount+1), rowCount)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "B"+strconv.Itoa(rowCount+1), index+1)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "C"+strconv.Itoa(rowCount+1), comparison.RequestTwo.Method)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "D"+strconv.Itoa(rowCount+1), comparison.RequestTwo.URL)
		excelWriteErrorCheck(err)
		err = xlsxFile.SetCellValue("Sheet1", "G"+strconv.Itoa(rowCount+1), authTwoNeeded)
		excelWriteErrorCheck(err)
		rowCount++
	}

	_ = xlsxFile.NewSheet("Sheet2")
	// fmt.Println("Sheets", sheets)

	err = xlsxFile.SetRowHeight("Sheet2", 1, 50)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetRowHeight("Sheet2", 2, 50)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetRowHeight("Sheet2", 3, 50)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet2", "A1", "D1", colorStyle)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet2", "A2", "A3", borderStyleVerticalAndHorizontalCenterStyle)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet2", "B2", "B3", borderStyleVerticalCenterStyle)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet2", "C2", "D3", borderStyleVerticalAndHorizontalCenterStyle)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellStyle("Sheet2", "D2", "D3", borderStyleVerticalCenterStyle)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetColWidth("Sheet2", "A", "A", 30)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetColWidth("Sheet2", "B", "B", 100)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetColWidth("Sheet2", "C", "C", 30)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetColWidth("Sheet2", "D", "D", 40)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellValue("Sheet2", "A1", "Authorization")
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "B1", "Auth URL")
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "C1", "Auth Header Key")
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "D1", "Auth Header Value")
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellValue("Sheet2", "A2", "Go Auth")
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "B2", authOneUrl)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "C2", authOneHeaderKey)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "D2", authOneHeaderValue)
	excelWriteErrorCheck(err)

	err = xlsxFile.SetCellValue("Sheet2", "A3", "Mule Auth")
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "B3", authTwoUrl)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "C3", authTwoHeaderKey)
	excelWriteErrorCheck(err)
	err = xlsxFile.SetCellValue("Sheet2", "D3", authTwoHeaderValue)
	excelWriteErrorCheck(err)

	err = xlsxFile.SaveAs("test.xlsx")
	if err != nil {
		log.Println("excel save", err)
		return
	}
}

func excelWriteErrorCheck(err error) {
	if err != nil {
		log.Panic("excel write", err)
	}
}
